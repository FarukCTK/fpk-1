#include <stdio.h>
#include <math.h>

#include "../HeaderFiles/UnitConverter.h"

int getIndexC(char input, char* Array);

void UnitConverter()
{
    char control = 'y';
    long double input;

    char unitChars[9] = {'p', 'n', 'u', 'm', 'b', 'k', 'M', 'G', 'T'};
    char unitNames[9][8] = {"Pico", "Nano", "Micro", "Milli", "", "Kilo", "Mega", "Giga", "Tera"};
    char customUnitName[10] = {'\0'};
    char inAndOutUnits[2] = {'\0'};

    while (control != 'x')
    {
        printf("\nWelcome to the unit converter. This program is Case-Sensitive so be careful!\n");
        printf("Give your SI Unit a name (Or use an existing one) -> "); 
        scanf("%s", customUnitName);

        printf("What is the value of your unit? -> ");
        scanf("%Lf", &input);

        printf("What is the SI power of your unit?\n"
               "(p)ico, (n)ano, (u)icro, (m)illi, (b)ase[=1], (k)ilo, (M)ega, (G)iga, (T)era\n"
               "    '-> ");

        while (1)
        {
            scanf(" %c", &inAndOutUnits[0]);
            if (getIndexC(inAndOutUnits[0], unitChars) != -1) break;
            else printf("Invalid input, try again! -> ");
        }

        printf("What is the target unit you'd like to convert to?\n"
               "    '-> ");
        
        while (1)
        {
            scanf(" %c", &inAndOutUnits[1]);
            if (getIndexC(inAndOutUnits[1], unitChars) != -1) break;
            else printf("Invalid input, try again! -> ");
        }

        printf("%3.8Lf %s%s = %3.8Lf %s%s:\n", 
                input, 
                    unitNames[getIndexC(inAndOutUnits[0], unitChars)], customUnitName, 
                input * pow(10, 3 * (getIndexC(inAndOutUnits[0], unitChars) - getIndexC(inAndOutUnits[1], unitChars))),
                    unitNames[getIndexC(inAndOutUnits[1], unitChars)], customUnitName);

        printf("(y) to use the calculator again\n"
               "(x) to exit -> ");
        scanf(" %c", &control);
    }
}

int getIndexC(char input, char* array)
{
    int i=0;
    for (i=0; array[i] != '\0'; i++)
    {
        if (array[i] == input) return i;
    }
    return -1;
}
