#include <stdio.h>
#include <stdlib.h>

#include "../HeaderFiles/TextEncryptorAndDecryptor.h"

int strilen(char* inp);
char *rotn(char *inp, int shift);

void TextEncryptorAndDecryptor()
{
    char control = 'y';
    char instring[64] = {'\0'};
    unsigned int rotamount;
    printf("Welcome to the ROT Text encryptor/decryptor!\n");

    while (control != 'x')
    {
        //This while loop below is crucial because if the user passes anything other
        //than e/E or d/D the rest will malfunction. It ensures the input is valid.
        printf("Would you like to (e)ncrypt or (d)ecrypt text -> ");
        while (1)
        {
            scanf(" %c", &control);
            if (control == 'e' || control == 'd' || control == 'E' || control == 'D') break;
            else printf("Invalid Input, try again -> ");
        }

        printf("by how many letters would you like to shift the text? -> ");
        while (1)
        {
            scanf("%u", &rotamount); getchar();
            if (rotamount <= 26 && rotamount >= 0) break;
            else printf("You can at most shift by 26 letters!\n"
                        "Also don't enter negative inputs!\n"
                        "Try again! -> ");
        }

        //if the user wants to decrypt, rotamount will be converted to negative.
        rotamount *= (control == 'e' || control == 'E') ? 1 : -1;

        printf("Now we will get the text\n  '-> ");
        fgets(instring, 64, stdin); instring[strilen(instring) - 1] = '\0';

        printf("\nHere is the converted text:\n%s\n", rotn(instring, rotamount));
        printf("\nWould you like to use this tool again?\n (x) to quit -> ");
        scanf(" %c", &control);
    }
}

#define IS_UPPER(c) c >= 'A' && c <= 'Z' ? 1 : 0
#define IS_LOWER(c) c >= 'a' && c <= 'z' ? 1 : 0

int strilen(char* inp)
{
    int i;
    for (i=0; inp[i] != '\0'; i++);
    return i;
}

char* rotn(char *inp, int n)
{
    int len, i;
    len = strilen(inp);
    char* des = (char*)calloc(len, sizeof(char));
    
    for (i=0; i<len; i++) 
    {
      if (IS_UPPER(inp[i])) des[i] = ((((inp[i] - 'A') + n) + 26) % 26) + 'A';
      else if (IS_LOWER(inp[i])) des[i] = ((((inp[i] - 'a') + n) + 26) % 26) + 'a';
      else des[i] = inp[i];
    }
    //printf("%s\n", des); uncomment this for debugging purposes.
    return des;
}



