#include <stdio.h>

#include "../HeaderFiles/AverageCalculator.h"

void AverageCalculator()
{
	float sum = 0, inp;
	int i, iterc;

	while (inp != 0)
	{
		printf("How many numbers have you got? -> ");
		scanf("%d", &iterc);
	
		for (i=0; i < iterc; i++)
		{
			printf("Enter %d. number: ", i+1);
			scanf("%f", &inp);
			sum += inp;
		}
	
		printf("The average of all those numbers is %.2f\n", (float)sum/iterc);
		printf("Would you like to use the calculator again? (1)\n(0) to exit   '-> ");
		scanf("%f", &inp);
	}

	printf("Exiting to the Utilities menu...\n");
}
