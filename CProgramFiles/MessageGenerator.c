#include <stdio.h>
#include <ctype.h>
#include <stdlib.h>
#include <string.h>

#include "../HeaderFiles/MessageGenerator.h"

#define IS_VALID_INPUT(a) (a == 'B' || a == 'L' || a == 'C' || a == 'A') ? 1 : 0
#define GET_INPUT_TO(a, b) fgets(a, b, stdin); a[strlen(a) - 1] = '\0'

#define PRINT_DASHLINE printf("\n- - - - - - - - - - - - - - - - -\n")

void BirthdayMessage(int* age, char* sName, char* rName);
void LateForWorkMessage(char* excuse, char* sName);
void Congratulations(char* sName, char* rName, char* cReason);
void Apology(char* sName, char* rName, char* aReason);

void MessageGenerator()
{
    char control = 'y';

    int* int1;
    char *rString1, *rString2, *rString3;
    
    while (control != 'x')
    {
        printf("Welcome to the message generator!\n"
               "What type of message would you like to generate?\n"
               "(B)irthday message, (L)ate for work message, (C)ongratulations, (A)pology\n     '-> ");

        while (1)
        {
            scanf(" %c", &control);
            getchar();
            control = toupper(control);
            if (IS_VALID_INPUT(control)) break;
            else printf("Invalid input, try again!\n    '-> ");
        }

        switch (control)
        {
            case 'B':
                int1 = (int*)malloc(sizeof(unsigned short int));
                rString1 = (char*)calloc(32, sizeof(char));
                rString2 = (char*)calloc(32, sizeof(char));

                printf("\nYou've chosen to write a birthday message.\n");
                printf("Input the receiver's age -> ");     scanf("%u", int1); getchar();
                printf("Input the receiver's name -> ");    GET_INPUT_TO(rString1, 32);
                printf("Input your name -> ");              GET_INPUT_TO(rString2, 32);
                BirthdayMessage(int1, rString2, rString1);

                free(int1); free(rString1); free(rString2);
                break;

            case 'L':

                rString1 = (char*)calloc(64, sizeof(char));
                rString2 = (char*)calloc(32, sizeof(char));

                printf("\nYou've chosen to write a late-for-work message.\n");

                printf("Input your excuse -> ");    GET_INPUT_TO(rString1, 64);
                printf("Input your name -> ");      GET_INPUT_TO(rString2, 32);
                LateForWorkMessage(rString1, rString2);

                free(rString1); free(rString2);
                break;
            
            case 'C':

                rString1 = (char*)calloc(32, sizeof(char));
                rString2 = (char*)calloc(32, sizeof(char));
                rString3 = (char*)calloc(64, sizeof(char));

                printf("You've chosen to write a congratulation message.\n");

                printf("Input the receiver's name -> ");        GET_INPUT_TO(rString1, 32);
                printf("Input your name -> ");                  GET_INPUT_TO(rString2, 32);
                printf("Input the congratulation reason -> ");  GET_INPUT_TO(rString3, 64);
                Congratulations(rString2, rString1, rString3);

                free(rString1); free(rString2); free(rString3);
                break;

            case 'A':

                rString1 = (char*)calloc(32, sizeof(char));
                rString2 = (char*)calloc(32, sizeof(char));
                rString3 = (char*)calloc(64, sizeof(char));

                printf("You've chosen to write an apology.\n");

                printf("Input the receiver's name -> ");        GET_INPUT_TO(rString1, 32);
                printf("Input your name -> ");                  GET_INPUT_TO(rString2, 32);
                printf("Input the apology reason -> ");  GET_INPUT_TO(rString3, 64);
                Apology(rString2, rString1, rString3);

                free(rString1); free(rString2); free(rString3);
                break;

            default:
                //there already is a validity check above so a default
                //case should not be triggered.
                printf("Weird error occurred, try again?\n");
                break;

        }

        printf("\nWould you like to use this tool again?\n  (x) to exit -> ");
        scanf(" %c", &control);

    }

    printf("Returning to the previous menu...\n");

}

void BirthdayMessage(int* age, char* sName, char* rName)
{
    PRINT_DASHLINE;
    printf("Dear %s,\n\nI wish you all the best on turning %d, here's to a healthy life!\n"
           "\nYours, %s.", rName, *age, sName);
    PRINT_DASHLINE;
}

void LateForWorkMessage(char* excuse, char* sName)
{
    PRINT_DASHLINE;
    printf("Morning boss,\n\n"
           "Unfortunately I'll be late to work today since %s, I hope you can excuse me.\n"
           "\nYours, %s.", excuse, sName);
    PRINT_DASHLINE;
}

void Congratulations(char* sName, char* rName, char* cReason)
{
    PRINT_DASHLINE;
    printf("Dear %s,\n\nI would like to congratulate you for %s.\n"
    "I wish you further luck on your future\n"
    "Yours, %s.", rName, cReason, sName);
    PRINT_DASHLINE;
}

void Apology(char* sName, char* rName, char* aReason)
{
    PRINT_DASHLINE;
    printf("Hello %s,\n\n"
           "I would like to apologize for %s\n"
           "I hope you can forgive me, for I want you to know that I feel very guilty.\n"
           "Yours, %s.", rName, aReason, sName);
    PRINT_DASHLINE;
}
