#include <stdio.h>
#include <time.h>
#include <stdlib.h>
#include <ctype.h>

#include "../HeaderFiles/RockPaperScissorsGame.h"

#define IS_VALID == 'r' || yourchoice == 'p' || yourchoice == 's'

char* getFull(char inp)
{
	switch (inp)
	{
		case 'r': return "ROCK";
		case 'p': return "PAPER";
		case 's': return "SCISSORS";
		default:  return "WEIRD_ERROR";
	}
}

void RockPaperScissorsGame()
{
	srand((unsigned)time(NULL));
	char yourchoice = 1, computerchoice;
	char computerpool[3] = {'r', 'p', 's'}; 
	
	while (yourchoice != 'x')
	{
		computerchoice= computerpool[rand() % 3];
		printf("\n\nThe computer chose at random! Now it's your turn!\n"
			"choose from\n"
			"[R]ock\n[P]aper\n[S]cissors\n (x to exit) '-> ");
		scanf(" %c", &yourchoice);
		yourchoice = tolower(yourchoice);
		if (yourchoice IS_VALID)
		{
			printf("\nYou chose %s, the computer chose %s\n", 
					getFull(yourchoice), getFull(computerchoice));

			if (yourchoice == computerchoice) printf("It's a Tie!\n"); 
			else switch (yourchoice)
			{
				case 'r':
					if (computerchoice == 's') printf("You Win!\n");
					else printf("You Lose!\n");
					break;
				case 'p':
					if (computerchoice == 's') printf("You Lose!\n"); 
					else printf("You win!\n"); 
					break;
				case 's':
					if (computerchoice == 'r') printf("You Lose!\n");
					else printf("You win!\n");
					break;
				default:
					printf("Weird Error Occured! Check the code!\n");
					break;
			}
			
		}
		else if (yourchoice == 'x') break;
		else printf("Invalid Input, try again!\n");
	}
	printf("Returning to the games menu...\n");
}
