#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#include "../HeaderFiles/NumberGuessingGame.h"

void NumberGuessingGame()
{
	int yourguess = -1;
	while (yourguess != 0)
	{
		srand((unsigned)time(NULL));
		int upperbound;
		while (1) 
		{
			printf("Choose an upper bound -> ");
			scanf("%d", &upperbound);
			if (upperbound >= 10 && upperbound <= 10000) break;
			else printf("Invalid number, please try again\n");
		}

		int counter;
		int randomnumber = (rand() % upperbound) + 1;
		printf("The random number has been chosen!\n");

		for (counter = 0; yourguess != randomnumber && yourguess != 0; counter++)
		{
			printf("Make a guess (0 to exit) -> ");
			scanf("%d", &yourguess);
			if (yourguess > upperbound || yourguess < 1) 
			{
				printf("Out Of Bounds! This attempt won't be counted.\n");
				--counter;
			}	
			else if (yourguess > randomnumber) printf("Lower!\n");
			else if (yourguess < randomnumber) printf("Higher!\n");
		}
		
		if (yourguess == 0)
		{
			printf("You've chosen to exit. Sending you back to the Games menu...\n");
			break;
		}
		else
		{
			printf("Congratulations! You did it! and it only took you %d tries.\n", counter);
			printf("You can try again (1) or quit to main menu (0) -> ");
			scanf("%d", &yourguess);
		}
		
	}
}
