#include <stdio.h>
#include <stdlib.h>

#include "../HeaderFiles/NumberGuessingGame.h"
#include "../HeaderFiles/RockPaperScissorsGame.h"
#include "../HeaderFiles/HangmanGame.h"

#include "../HeaderFiles/AverageCalculator.h"
#include "../HeaderFiles/UnitConverter.h"
#include "../HeaderFiles/TextEncryptorAndDecryptor.h"

#include "../HeaderFiles/MessageGenerator.h"

#define PRINT_DASHLINE printf("\n\n- - - - - - - - - - - - - - - - -\n\n")

//the three sub-control functions. they act as menus and work in the same manner as main()
void GamesControl();
void UtilitiesControl();
void FluffControl();

int main()
{
	int control = 1;

	while (control != 0)
	{
		PRINT_DASHLINE;
		printf("Welcome to FPK-1!\n"
			"Here is a list of what you can do:\n"
			"\n"
			"(1) Games\n"
			"(2) Utilities\n"
			"(3) Fluff\n"
			"\n"
			"(0) to exit\n"
			"   '-> ");
		scanf("%d", &control);
		switch (control)
		{
			case 1: GamesControl(); 							break;
			case 2: UtilitiesControl(); 						break;
			case 3: FluffControl(); 							break;
			case 0: printf("See you again soon!\n"); 			break;
			default: printf("Invalid choice, try again!\n\n"); 	break;
		}
	}

	return 0;
}

void GamesControl()
{
	int control = 1;
	while (control != 0)
	{
		PRINT_DASHLINE;
		printf("Here is what you can play:\n"
			"\n"
			"(1) Number Guessing Game\n"
			"(2) Rock Paper Scissors Game\n"
			"(3) Hangman Game\n"
			"\n"
			"(0) to go back\n"
			"   '-> ");
		scanf("%d", &control);
		switch (control)
		{
			case 1: NumberGuessingGame(); 				break;
			case 2: RockPaperScissorsGame();			break;
			case 3: HangmanGame();						break;

			case 0: printf("Returning back to the main menu...\n"); break;
			default: printf("Invalid input, try again!\n\n"); 	break;
		}
	}
}

void UtilitiesControl()
{
	int control = 1;
	while (control != 0)
	{
		PRINT_DASHLINE;
		printf("Here is what you can use:\n"
			"\n"
			"(1) Average Calculator\n"
			"(2) Unit Converter\n"
			"(3) Text encryptor and decryptor\n"
			"\n"
			"(0) to go back\n"
			"   '-> ");
		scanf("%d", &control);
		switch (control)
		{
			case 1: AverageCalculator();			 				break;
			case 2: UnitConverter();								break;
			case 3: TextEncryptorAndDecryptor();					break;

			case 0: printf("Returning back to the main menu...\n"); break;
			default: printf("Invalid input, try again!\n\n"); 		break;
		}
	}
}

void FluffControl()
{
	int control = 1;
	while (control != 0)
	{
		PRINT_DASHLINE;
		printf("Here is what you can use:\n"
			"\n"
			"(1) Message Generator\n"
			"\n"
			"(0) to go back\n"
			"   '-> ");
		scanf("%d", &control);
		switch (control)
		{
			case 1: MessageGenerator();				 				break;
			
			case 0: printf("Returning back to the main menu...\n"); break;
			default: printf("Invalid input, try again!\n\n"); 		break;
		}
	}
}
