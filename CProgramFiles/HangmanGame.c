#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <ctype.h>

#include "../HeaderFiles/HangmanGame.h"

#define TEXTSIZE 64
#define IS_LETTER(a) (a >= 'a' && a <='z') ? 1 : 0

int popNewline = 0;

//FUNCTION DECLEARATIONS
int getAmountOfWords(FILE* filePtr, char* writeTo);
void chooseRandomWord(FILE* filePtr, char* writeTo, int fileLength);
void updateHangmanState(FILE* filePtr, char** hangmanStateString);
void printHangmanState(char** string, char* rString);


static inline void initializeDisplayString(char* dString, char* cString);
int mirrorValue(char valueToMirror, char* srcArray, char* destArray);

void HangmanGame()
{
    //VARIABLE DECLEARATIONS
    char* displayString = (char*)calloc(TEXTSIZE, sizeof(char));
    char* readString    = (char*)calloc(TEXTSIZE, sizeof(char));

    char** hangmanState = (char**)calloc(5, sizeof(char*));
    for (int i=0; i<5; i++) hangmanState[i] = (char*)calloc(32, sizeof(char));

    FILE* wordFilePtr;
    FILE* hStateFilePtr;

    char control = 'y';
    char guess;
    int amountOfWords;
    int numberOfAttempts;

    popNewline = 1;

    numberOfAttempts = 6;
    wordFilePtr   =   fopen("./FilesUsedByPrograms/HangmanWords.txt", "r");
    hStateFilePtr =   fopen("./FilesUsedByPrograms/HangmanImages.txt", "r");
    //this is where the game begins
    if (wordFilePtr == NULL)
    {
        printf("Error opening file!\n");
    }

    else
    {
        //this will be commented out later
        amountOfWords = getAmountOfWords(wordFilePtr, readString);
        printf("Your file has %d lines\n", amountOfWords);

        chooseRandomWord(wordFilePtr, readString, amountOfWords);
        // Uncomment for debugging
        // printf("Random chosen word: %s\n", readString); 

        initializeDisplayString(displayString, readString);
        updateHangmanState(hStateFilePtr, hangmanState);

        //This is where the fun begins
        while (numberOfAttempts > 0)
        {
            printHangmanState(hangmanState, displayString);
            printf("Choose a letter -> ");
            //another while loop to ensure valid input
            while (1)
            {
                scanf(" %c", &guess);
                guess = tolower(guess);
                if (guess >= 'a' && guess <= 'z') break;
                else printf("Invalid input, try again -> ");
            }
            if (!mirrorValue(guess, readString, displayString))
            {
                --numberOfAttempts;
                updateHangmanState(hStateFilePtr, hangmanState);
            }
            if (mirrorValue('_', displayString, displayString) == 0)
            {
                break;
            }
        }

        readString[strlen(readString)-1]='\n';
        if (numberOfAttempts <= 0)
        {
            popNewline = 1;
            updateHangmanState(hStateFilePtr, hangmanState);
            printHangmanState(hangmanState, readString);

            printf("Game Over!");
        }
        else if (mirrorValue('_', displayString, displayString) == 0)
        {
            printHangmanState(hangmanState, readString);
            printf("You win!");
        }

        fclose(wordFilePtr);
        fclose(hStateFilePtr);
    }
}

// - - -- - - - FOUR METHODS BELOW FOR WORD STATE MANUPILATION - - - - -//
int getAmountOfWords(FILE* filePtr, char* writeTo)
{
    int counter=0;
    while (!feof(filePtr))
    {
        fgets(writeTo, TEXTSIZE, filePtr);
        ++counter;    
    }
    rewind(filePtr);
    return counter;
}

void chooseRandomWord(FILE* filePtr, char* writeTo, int fileLength)
{
    srand((unsigned)time(NULL));
    int counter = 0, randint = rand() % fileLength;

    while (!feof(filePtr))
    {
        fgets(writeTo, TEXTSIZE, filePtr);
        if (counter == randint) break;

        ++counter;
    }
    rewind (filePtr);
}

void updateHangmanState(FILE* filePtr, char** hangmanStateString)
{
    popNewline = 0;
    for (int i=0; i<5; i++)
    {
        fgets(hangmanStateString[i], 32, filePtr);
    }
    //we don't want to rewind here because the state will only get updated.
}

void printHangmanState(char** hString, char* dString)
{
    for (int i=0; i<5; i++) 
    {
        if (i == 3)
        {
            //pop the newline character and print the word afterward.
            if (!popNewline) 
            {
                hString[i][strlen(hString[i]) - 1] = '\0';
                ++popNewline;
            }
            printf("%s   %s", hString[i], dString);
        }

        else printf("%s", hString[i]);
    }
}
//- - - - - - - - - - - - - END - - - - - - - - - - - - -//


//- - - SOME MORE METHODS THAT WE WILL USE TO MANIPULATE THE ACTUAL WORD AND THE DISPLAY STRING - - -//
static inline void initializeDisplayString(char* dString, char* cString)
{
    int i;
    for (i=0; cString[i] != '\0'; i++) 
    {
        if (cString[i] >= 'a' && cString[i] <= 'z') dString[i] = '_';
        else if (cString[i] = ' ') dString[i] = ' ';
    }
    dString[i] = '\n';
}

int mirrorValue(char valueToMirror, char* srcArray, char* destArray)
{
    int isInString = 0;
    for (int i=0; srcArray[i] != '\0'; i++)
    {
        if (srcArray[i] == valueToMirror)
        {
            destArray[i] = srcArray[i];
            ++isInString;
        }
    }
    return isInString;
}




